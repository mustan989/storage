package storage

type Option func(s *FileStorage)

func WithRoot(root string) Option {
	return func(s *FileStorage) {
		s.root = root
	}
}

func WithURL(url string) Option {
	return func(s *FileStorage) {
		s.url = url
	}
}

func WithConfig(config *Config) Option {
	return func(s *FileStorage) {
		s.root = config.Root
		s.url = config.URL
	}
}
