package storage_test

import (
	"bytes"
	"gitlab.com/mustan989/storage"
	"io"
	"log"
	"os"
	"strings"
	"testing"
)

const (
	storageRoot = "data"

	fileName    = "hello_world.txt"
	fileContent = "hello world!"
	fileURL     = "test/file"
	fileDir     = storageRoot + "/" + fileURL
	filePath    = fileDir + "/" + fileName
)

var (
	s       = storage.NewFileStorage(storage.WithRoot(storageRoot))
	content = []byte(fileContent)
)

func TestStorage_Put(t *testing.T) {
	// remove all created directories after finish
	defer os.RemoveAll(storageRoot)

	// put with storage
	if err := s.Put(fileName, fileURL, content); err != nil {
		t.Fatalf("error putting file in storage: %s", err)
	}

	// open created file
	file, err := os.Open(filePath)
	if err != nil {
		t.Fatalf("error opening expected file: %s", err.Error())
	}
	defer file.Close()
	b, err := io.ReadAll(file)
	if err != nil {
		t.Fatalf("error reading expected file: %s", err.Error())
	}

	// compare contents
	if bytes.Compare(b, content) != 0 {
		t.Fatal("original and expected contents are not equal", string(content), string(b))
	}
}

func TestStorage_Get(t *testing.T) {
	// create directory and write file
	if err := os.MkdirAll(fileDir, 0744); err != nil {
		t.Fatalf("error creating file directory: %s", err.Error())
	}
	defer os.RemoveAll(storageRoot)

	file, err := os.Create(filePath)
	if err != nil {
		t.Fatalf("error creating file: %s", err.Error())
	}
	if _, err = file.Write(content); err != nil {
		t.Fatalf("error writing to file: %s", err.Error())
	}
	if err := file.Close(); err != nil {
		t.Fatalf("error closing file: %s", err.Error())
	}

	// get with storage
	name, c, err := s.Get(fileURL)
	if err != nil {
		t.Fatalf("error getting file from storage: %s", err.Error())
	}

	// compare contents
	if strings.Compare(fileName, name) != 0 || bytes.Compare(content, c) != 0 {
		t.Fatal("original file name and content are not equal", fileName, name, string(content), string(c))
	}
}

func TestStorage_Delete(t *testing.T) {
	// create directory and write file
	if err := os.MkdirAll(fileDir, 0744); err != nil {
		t.Fatalf("error creating file directory: %s", err.Error())
	}
	defer os.RemoveAll(storageRoot)

	file, err := os.Create(filePath)
	if err != nil {
		t.Fatalf("error creating file: %s", err.Error())
	}
	if _, err = file.Write(content); err != nil {
		t.Fatalf("error writing to file: %s", err.Error())
	}
	if err := file.Close(); err != nil {
		t.Fatalf("error closing file: %s", err.Error())
	}

	// delete with storage
	if err := s.Delete(fileURL); err != nil {
		t.Fatalf("error deleting file: %s", err.Error())
	}

	// check if deleted
	if _, err := os.Stat(filePath); !os.IsNotExist(err) {
		if err == nil {
			t.Fatal("error checking file: file exists")
		} else {
			log.Fatalf("error checking file: %s", err.Error())
		}
	}
}

func TestStorage_Replace(t *testing.T) {
	// data to be replaced
	orig := []byte("hell in world!")

	// create directory and write file
	if err := os.MkdirAll(fileDir, 0744); err != nil {
		t.Fatalf("error creating file directory: %s", err.Error())
	}
	defer os.RemoveAll(storageRoot)

	origFile, err := os.Create(filePath)
	if err != nil {
		t.Fatalf("error creating file: %s", err.Error())
	}
	if _, err = origFile.Write(orig); err != nil {
		t.Fatalf("error writing to file: %s", err.Error())
	}
	if err := origFile.Close(); err != nil {
		t.Fatalf("error closing file: %s", err.Error())
	}

	// replace with storage
	if err := s.Replace(fileName, fileURL, content); err != nil {
		t.Fatalf("error replacing file: %s", err.Error())
	}

	// open created file
	file, err := os.Open(filePath)
	if err != nil {
		t.Fatalf("error opening expected file: %s", err.Error())
	}
	defer file.Close()
	b, err := io.ReadAll(file)
	if err != nil {
		t.Fatalf("error reading expected file: %s", err.Error())
	}

	// compare contents
	if bytes.Compare(b, content) == 0 {
		t.Fatal("original and expected contents are equal", string(content), string(b))
	}
}

func TestStorage_PutGetReplaceDelete(t *testing.T) {
	// data to be replaced
	orig := []byte("hell in world!")

	// remove all created directories after finish
	defer os.RemoveAll(storageRoot)

	// put with storage
	if err := s.Put(fileName, fileURL, orig); err != nil {
		t.Fatalf("error putting file in storage: %s", err)
	}

	// get with storage
	origName, origC, err := s.Get(fileURL)
	if err != nil {
		t.Fatalf("error getting file from storage: %s", err.Error())
	}

	// compare contents
	if strings.Compare(fileName, origName) != 0 || bytes.Compare(orig, origC) != 0 {
		t.Fatal("original file name and content are not equal", fileName, origName, string(orig), string(origC))
	}

	// replace with storage
	if err := s.Replace(fileName, fileURL, content); err != nil {
		t.Fatalf("error replacing file: %s", err.Error())
	}

	// get with storage
	_, c, err := s.Get(fileURL)
	if err != nil {
		t.Fatalf("error getting file from storage: %s", err.Error())
	}

	// compare contents
	if bytes.Compare(c, content) == 0 {
		t.Fatal("original and expected contents are equal", string(content), string(c))
	}

	// delete with storage
	if err := s.Delete(fileURL); err != nil {
		t.Fatalf("error deleting file: %s", err.Error())
	}
}
