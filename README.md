# storage

## Description

This is example of simple file storage

## Usage

### Get Package

Run:

```shell
go get -u gitlab.com/mustan989/storage
```

### In Code

Example:

```go
package main

import (
	"fmt"
	"gitlab.com/mustan989/storage"
)

func main() {
	s := storage.NewFileStorage(storage.WithRoot("/data"))

	data := []byte("hello world!")
	err := s.Put("example.txt", "examples/example", data)
	if err != nil {
		panic(err)
	}

	name, content, err := s.Get("examples/example")
	if err != nil {
		panic(err)
	}
	fmt.Printf("file name: %s\ncontent: %s", name, string(content))
}
```