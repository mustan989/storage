package storage

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Root string `json:"root" yaml:"root" xml:"root"`
	URL  string `json:"url" yaml:"url" xml:"url"`
}

// ParseConfigFile parses file located at path and returns *Config or error if any occurs. Format can be YAML, JSON or XML
func ParseConfigFile(path string, format int) (*Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	cfg := &Config{}
	switch format {
	case YAML:
		if err := yaml.NewDecoder(file).Decode(cfg); err != nil {
			return nil, err
		}
	case JSON:
		if err := json.NewDecoder(file).Decode(cfg); err != nil {
			return nil, err
		}
	case XML:
		if err := xml.NewDecoder(file).Decode(cfg); err != nil {
			return nil, err
		}
	default:
		return nil, errors.New("unknown file format")
	}
	return cfg, nil
}

const (
	YAML = iota
	JSON
	XML
)
