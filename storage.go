package storage

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

var (
	ErrFileExists    = errors.New("file already exists")
	ErrFileNotExists = errors.New("file not exists")
	ErrIsNotDir      = errors.New("file is not directory")
)

type Storage interface {
	Put(name, url string, content []byte) error
	Get(url string) (name string, content []byte, err error)
	Delete(url string) error
	Replace(name, url string, content []byte) error
	GetChildren(url string) (urls []string, err error)
}

type FileStorage struct {
	root string
	url  string
}

func (s *FileStorage) Put(name, url string, content []byte) error {
	url = s.trimURL(url)
	path := fmt.Sprintf("%s/%s", s.root, url)
	current, err := getFileName(path)
	if err != nil && !errors.Is(err, ErrFileNotExists) {
		return err
	} else if len(current) != 0 {
		return ErrFileExists
	}
	if err := os.MkdirAll(path, 0744); err != nil {
		return err
	}
	path = fmt.Sprintf("%s/%s", path, name)
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.Write(content)
	if err != nil {
		return err
	}
	return nil
}

func (s *FileStorage) Get(url string) (name string, content []byte, err error) {
	url = s.trimURL(url)
	path := fmt.Sprintf("%s/%s", s.root, url)
	name, err = getFileName(path)
	if err != nil {
		return
	}
	path = fmt.Sprintf("%s/%s", path, name)
	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()
	content, err = io.ReadAll(file)
	if err != nil {
		return
	}
	return
}

func (s *FileStorage) Delete(url string) error {
	url = s.trimURL(url)
	path := fmt.Sprintf("%s/%s", s.root, url)
	name, err := getFileName(path)
	if err != nil {
		return err
	}
	path = fmt.Sprintf("%s/%s", path, name)
	if err := os.Remove(path); err != nil {
		return err
	}
	return nil
}

func (s *FileStorage) Replace(name, url string, content []byte) error {
	if err := s.Delete(url); err != nil {
		return err
	}
	return s.Put(name, url, content)
}

func (s *FileStorage) GetChildren(url string) (urls []string, err error) {
	url = s.trimURL(url)
	path := fmt.Sprintf("%s/%s", s.root, url)
	if !isExists(path) {
		return nil, ErrFileNotExists
	}
	info, err := os.Stat(path)
	if err != nil {
		return nil, err
	}
	if !info.IsDir() {
		return nil, ErrIsNotDir
	}
	entries, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}
	urls = make([]string, 0)
	for _, entry := range entries {
		if entry.IsDir() {
			u := fmt.Sprintf("%s/%s", url, entry.Name())
			urls = append(urls, u)
		}
	}
	return urls, err
}

func (s *FileStorage) trimURL(url string) string {
	if strings.HasPrefix(url, s.url) {
		return strings.TrimLeft(url, fmt.Sprintf("%s/", s.url))
	}
	return url
}

// getFileName returns first file name found in dir or ErrFileNotExists if none is found
func getFileName(dir string) (string, error) {
	if !isExists(dir) {
		return "", ErrFileNotExists
	}
	entries, err := os.ReadDir(dir)
	if err != nil {
		return "", err
	}
	for _, entry := range entries {
		if !entry.IsDir() {
			return entry.Name(), nil
		}
	}
	return "", ErrFileNotExists
}

// isExists returns true if file or directory at path exists, else returns false
func isExists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

func NewFileStorage(opts ...Option) Storage {
	s := defaultFileStorage()
	for _, opt := range opts {
		opt(s)
	}
	return s
}

func defaultFileStorage() *FileStorage {
	return &FileStorage{
		root: "data",
		url:  "storage/data",
	}
}
